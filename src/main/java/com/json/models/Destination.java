package com.json.models;

import java.util.ArrayList;

public class Destination {
	 ArrayList<Hotels> mallorca = new ArrayList<Hotels>();
	  ArrayList<Hotels> tyrol = new ArrayList<Hotels>();
	  
	public ArrayList<Hotels> getMallorca() {
		return mallorca;
	}
	public void setMallorca(ArrayList<Hotels> mallorca) {
		this.mallorca = mallorca;
	}
	public ArrayList<Hotels> getTyrol() {
		return tyrol;
	}
	public void setTyrol(ArrayList<Hotels> tyrol) {
		this.tyrol = tyrol;
	}

}
