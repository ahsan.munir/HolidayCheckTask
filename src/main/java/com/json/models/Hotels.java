package com.json.models;

public class Hotels {
	 String name;
	 String destination="";
     int stars;
     public Hotels() {};
  	 public Hotels(String name, int stars, String destination) {
		super();
		this.name = name;
		this.stars = stars;
		this.destination = destination;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public int getStars() {
		return stars;
	}
	public void setStars(int stars) {
		this.stars = stars;
	}
  	 
}
