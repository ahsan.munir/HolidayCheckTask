package com.main.classes;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.json.models.Destination;
import com.json.models.Hotels;

public class Main {

	public static void main(String[] args) throws StreamReadException, DatabindException, IOException {
		try {
			//Using java Object Mapper to Map JSON
			ObjectMapper mapper = new ObjectMapper();
			InputStream inputStream = new FileInputStream(
					new File(System.getProperty("user.dir") + "/src/main/resources/Input.json"));
			//Creating a type Reference
			TypeReference<List<Destination>> typeReference = new TypeReference<List<Destination>>() {};
			//Loading a Json in the list of Destination Model
			List<Destination> places = mapper.readValue(inputStream, typeReference);
			ArrayList<Hotels> filteredList = new ArrayList<Hotels>();
			for (int i = 0; i < places.get(0).getMallorca().size(); i++) {
				if (args[0].equals(Integer.toString(places.get(0).getMallorca().get(i).getStars())))
				{
					filteredList.add(new Hotels(places.get(0).getMallorca().get(i).getName(), places.get(0).getMallorca().get(i).getStars(),"Mollorca"));
				}
			}
			for (int i = 0; i < places.get(0).getTyrol().size(); i++) {
				if (args[0].equals(Integer.toString(places.get(0).getTyrol().get(i).getStars()))) {
					filteredList.add(new Hotels(places.get(0).getTyrol().get(i).getName(), places.get(0).getTyrol().get(i).getStars(), "Tyrol"));
				}
			}
			//Writing Output file to local Repository. 
			mapper.writeValue(new File(System.getProperty("user.dir") + "/src/main/resources/Output.json"),
					filteredList);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
